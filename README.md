# Project 4: Brevet time calculator with Ajax
# Co-Author: Ahmed Al Ali

Reimplement the RUSA ACP controle time calculator with flask and ajax.


UPDATE: The code now works as expected, anything that would result into an error would naturally return the start time for BOTH the Open Time and Close Time.

Includes:

. control distance greater than brevet distance by more than 20%

. Any number that is out of bounds or any case not presented in brevet distances between 0 -> 1000


The rules are implemented as requested by the rule handbook to the best of the understanding.

Test cases have been passed.